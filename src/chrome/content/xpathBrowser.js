/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is XPather - a firefox extension.
 *
 * The Initial Developer of the Original Code is
 * Viktor Zigo <xpather@alephzarro.com>.
 * Portions created by the Initial Developer are Copyright (C) 2005-2008
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */
 
 /* XPather,  Author: Viktor Zigo, http://xpath.alephzarro.com */

var treeView = {
    results: new Array(),
    fullXPaths: new Array(),
    contentText: new Array(),
    initialize: function(resultDOMNodes){
        var oldsize = this.rowCount;
        this.results = resultDOMNodes;
        this.fullXPaths = new Array();
        this.contentText = new Array();
        //not avail. the first time
        if (this.treeBox) {
            this.treeBox.rowCountChanged(0,0-oldsize);
            this.treeBox.rowCountChanged(0,this.rowCount);
            //TODO - can we invalidate the tree otherwise ??? 
            //this.treeBox.invalidate(); //does not work - it refreshes existing rows ...but does not change count of rows
        }
    },
    get rowCount() { 
        return this.results.length; },
        
    getCellText : function(row,aColumn){
        //log("aa:"+aColumn+"type:" + typeof(aColumn)+"  str:"+(aColumn instanceof string));
        var column = (aColumn.id) ? aColumn.id : aColumn; //Firefox pre1.5 compatibility
        //log("bb:"+column);
        if(!this.results[row]) return 'err#'+row+':'+column;
        if (column== "idColNo") return row + 1;
        if (column == "idColContent"){
            var txt = this.contentText[row];
            if (!txt) {
                //full path up to the root!!
                txt = polishText(this.results[row].textContent);
                this.contentText[row]=txt;
            }
            return txt;
        }
        if (column == "idColFullxpath") {
            var xpat = this.fullXPaths[row];
            if (!xpat) {
                //full path up to root!!
                xpat = generateXPath(this.results[row], null, expatState.getDefaultNS(), prepareKwds() );
                this.fullXPaths[row]=xpat;
            }
            return xpat;
        }
        else return 'todo...';
    },
    doInvalidate:function(){
        this.fullXPaths = new Array();
        this.treeBox.invalidate();
    },
    setTree: function(treebox){ this.treeBox = treebox; },
    isContainer: function(row){ return false; },
    isSeparator: function(row){ return false; },
    isSorted: function(row){ return false; },
    getLevel: function(row){ return 0; },
    getImageSrc: function(row,col){ return null; },
    getRowProperties: function(row,props){},
    getCellProperties: function(row,col,props){},
    getColumnProperties: function(colid,col,props){},
    cycleHeader: function(colid,elt){},
    cycleCell: function(rowid,colid){}
};

//removes all new lines and crop to 200
function polishText(aString){
    var str = '';
    var ch;
    for( i in aString){
        ch = aString[i];
        if (ch=='\n') str+=' ';
        else if (ch=='\t') str+=' ';
        else str+=ch;
            
        if (i>200) {
            str+='...';
            break;
        }
    }
    return str;
}
//expands \n etc. to real chars
function expandString(aText){
    var s = aText.replace(/"/g, '\\"');
    return eval('"'+s+'"');
}

//returns a list of nodes from all selections
function flattenSelection(tree){
    var outNodeList=[];
    var start = new Object();
    var end = new Object();
    var numRanges = tree.view.selection.getRangeCount();
    for (var t=0; t<numRanges; t++){
        tree.view.selection.getRangeAt(t,start,end);
        for (var i=start.value; i<=end.value; i++){
            outNodeList.push(treeView.results[i]);
        }
    }
    return outNodeList;
}

function concatenateForEach(nodeList, nodeFunct, separator){
    var str='';
    for (var i=0; i<nodeList.length; i++){
        if (i>0) str+=separator;
        str+=nodeFunct(nodeList[i])
    }
    return str;
}

function extractText(aNode){
    return aNode.textContent;
}

// return full XPath and optionaly also relative XPath of a given node
function extractXPaths(aNode){
    var idx = -1;
    var res =treeView.results;
    for (var i in res )
        if(res[i]==aNode){
            idx=i;
            break;
        }
    if (idx>=0){
        var s = treeView.fullXPaths[idx];
        //generate also the relative xpath if the feature is on
        var ctxtNode = expatState.getContextNodeNull();
        if (ctxtNode) 
            s+='\n' + generateXPath(aNode, ctxtNode, expatState.getDefaultNS(), prepareKwds() );
        return s;
    }
    else return "";
}

function extractInnerHTML(aNode){
    if (!aNode.innerHTML){
        return new XMLSerializer().serializeToString(aNode);
    }
    else return aNode.innerHTML;
}

// performs a regexp based substitution if appropriate
function extractAndTransformText(aNode){
    var txt = extractText(aNode);
    return (isRegexpView())
        ? transformContent(txt)
        : txt;
}

function removeAllChildren(aNode) {
    var firstchild = aNode.firstChild;
    while(firstchild){
        aNode.removeChild(firstchild);
        firstchild = aNode.firstChild;
    }
}

function copyChilds(fromParent,toParent){
    var fromCh = fromParent.childNodes;
    for (var i in fromCh) {
        toParent.appendChild( fromCh[i].cloneNode(true) );
    }
} 

function onSelect(tree){
    var nodeList = flattenSelection(tree);
    
    //update oll result tabs
    if (nodeList.length>0){
        //use default separator only if subst is not defined
        var textSeparator = (isRegexpView() && getSubstition()) ? '' : ', ';
        
        //set the content
        document.getElementById('contentText').value = concatenateForEach(nodeList, extractAndTransformText, textSeparator);
        document.getElementById('contentInnerHTML').value = concatenateForEach(nodeList, extractInnerHTML, '\n<!-- next result-->\n');
        document.getElementById('contentXPaths').value = concatenateForEach(nodeList, extractXPaths, '\n');
        
        //append DOM of all matching nodes
        var renderingFrame = document.getElementById('contentRendered');
        var body = renderingFrame.contentDocument.body;
        removeAllChildren(body);
        
        var doc = expatState.getFrameDocRoot();
        if (! (doc instanceof HTMLDocument)) return;

        
        for (var i in nodeList) {
            var clone = renderingFrame.contentDocument.importNode(nodeList[i], true );
            body.appendChild( clone );
        }
    
        //show the frame preview if it was still hidden
        if (renderingFrame.collapsed) {
            document.getElementById('dummyClipp').collapsed=true;
            renderingFrame.collapsed=false;
        }            
         //blink only if exactly one node is selected
        if (nodeList.length==1) flashNode(nodeList[0] );
        return;
    }
    else {
        document.getElementById('contentText').value ='';
        document.getElementById('contentInnerHTML').value = '';
        document.getElementById('contentXPaths').value = '';
        //delete all rendered children
        var renderingFrame = document.getElementById('contentRendered');
        var body = renderingFrame.contentDocument.body;
        removeAllChildren(body);
    }
} 

//handler of the XPath Menu settings (called from toolbar/xpather.js), Polymorhic.
function onSettingChanged(){
    treeView.doInvalidate();
    //displayXPath();
} 

//access to global data exchange (copy)
var expatState;

function onWebClipTabSelection(){
    //alert("Render tab selected");
    //TODO lazy ducument load
}
function onInfoTabSelection(){
    updateInfo();
}

function doTabDispatch(aSelectedTabIndex){
    switch(aSelectedTabIndex){
        case 2: onWebClipTabSelection();break;
        case 4: onInfoTabSelection();break;
    }
}

function preLoadDocument(){
    var doc = expatState.getFrameDocRoot();
    if (! (doc instanceof HTMLDocument)){
        var msg=getBundle().getFormattedString("wrnWebclipCannotload", [doc])
        addToInfo(msg);
        log(msg);
        return;
    }
    var url = doc.documentURI;
    if (!url) {
        log("There is no url of the document to be loaded into web-clipping area.");
        return;
    }
    var renderingNode = document.getElementById('contentRendered');
    if (renderingNode.contentDocument.location.href!=url)
    {
        var docShell = renderingNode.docShell;
        docShell.allowAuth = false
        docShell.allowJavascript = false
        docShell.allowMetaRedirects = false
        docShell.allowPlugins = false
        docShell.allowSubframes = false
        //renderingNode.setAttribute('src', urll);
        renderingNode.contentDocument.location.replace(url);
    }        
} 

//update the entire xpathBrowser, called from init() or any clinet
function updateWindow(anExpatState, inheritedCrossFrame){
    //update inheritable settings
    setAttrValue('cmd:toggleCrossFrame',"checked", inheritedCrossFrame);
    
    expatState = anExpatState;
    var xpath = expatState.getXPath();
    if (!xpath){
        xpath = generateXPath(expatState.getCurrentNode(), expatState.getContextNodeNull(), 
            expatState.getDefaultNS(), prepareKwds());
        if (!xpath) alert(getBundle().getString("xpather.errUpdate"));
    }
    document.getElementById('xpatherText').value = xpath;
    
    evalIt();
    
    //preloads the rendered webpage  //TODO: make it lazy loading
    preLoadDocument();
} 

function updateInfo(){
    var txt="";
    //TODO localize - once stabilized :)
    txt+="Document root URI:\n"+expatState.getDocRoot().documentURI;
    txt+="\n\nActual frame URI:\n"+expatState.getFrameDocRoot().documentURI;
    txt+="\n";
    var frm =expatState.getFrames().frames;
    txt+="\nAll frame windows:"+frm.length;
    var kwds = prepareKwds();
    for (var i=0 ; i<frm.length; i++) {
        txt+= "\n "+i+". URI="+frm[i].frameUri+"   XPath="+frm[i].getXPath(kwds);
    }
    txt+="\n";
    
    //current node NSRESOLVER
    //var currNode = expatState.getCurrentNode();
    var currNode = getNSResolutionNode();
    txt+="\nSelected node (used as NS resolver):\n";
    txt+= currNode ? 
        generateXPath(currNode, expatState.getContextNodeNull(), expatState.getDefaultNS(), kwds) : "null";
    
    txt+="\nDefault namespace:\n";
    txt+= expatState.getDefaultNS();
    
    txt+="\n";
    document.getElementById('contentInfo').value = txt;
}

function addToInfo(aText){
    var textBox = document.getElementById('contentInfo');
    var oldtxt = textBox.value;
    textBox.value = oldtxt+aText+"\n";
}

function init(){
    //exchange data with the main window (could not do it with window.arguments, cuz it contained no data the next time it was opened)
    var newExpatState = window.opener.expatState;
    if (expatState!=newExpatState){
        updateWindow(newExpatState, window.opener.isCrossFrame());
    }
    else updateInfo();
    
    //adjust toolbar "preferences"
    setAttrValue("br_showParentView", "collapsed", !isParentView() );
    setAttrValue("br_showRegexpView", "collapsed", !isRegexpView() );
    
    setAttrValue("idResultsSelect", "disabled", true);
    setAttrValue("idResultsBrowse", "disabled", true);
}

//filter a list of aGoundNodes, return those with textContent matching regexpText
function filterResults(aFoundNodes){
    var text = document.getElementById('regexpText').value;
    var re = getValidRegexp(text);
    if (!re) return aFoundNodes;
    var filteredNodes = [];
    for (var i in aFoundNodes){
        var content = extractText(aFoundNodes[i]);
        if (re.test(content)) filteredNodes.push(aFoundNodes[i]);  
    }
    return filteredNodes;
} 

//make a substitution of aText using regexpText and subtText, if all of them are not null 
function transformContent(aText){
    var subst = getSubstition();
    var re = getValidRegexp(document.getElementById('regexpText').value); 
    if (!re || !subst || !aText) return aText;
    try{
        var expandedSubst = expandString(subst);
        return aText.replace(re, expandedSubst);
    }
    catch(e){
        return "[SUBST ERROR]:"+e+" \n"+aText;
    }
}

//evaluation of the given xpath (parent xpath, regexp)
function evalIt(){
    //evaluate xpath
    var xpath = document.getElementById('xpatherText').value;
    //log('EVAL:bowser '+expatState.getContextNode()+'    '+ xpath);
    var foundNodes;
    if(isCrossFrame()){
        //log("CROSSFRAME");
        foundNodes = evaluateXPathFrames(xpath);
    }
    else {
        foundNodes = evaluateXPath(expatState.getContextNode(), xpath);
    }
    
    var lengthText = "0";
    if (foundNodes) {
       lengthText = foundNodes.length;
        //regexp filtration 
        if (isRegexpView()){
            foundNodes = filterResults(foundNodes);
            lengthText = foundNodes.length + " from " + lengthText+ (isCrossFrame() ? ' CROSS-FRAME':'');
        }
    }
    else foundNodes = [];
    document.getElementById('idTotalCount').value = lengthText;
    treeView.initialize(foundNodes);
    
      /*var tree = document.getElementById("idExpTree");
      var boxobject = tree.boxObject;
      boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
      boxobject.invalidate();
     */
    document.getElementById('idExpTree').view = treeView; 
    document.getElementById('idExpTree').focus();
}

// returns either valid RegExp object or null (and explanation set to aTextbox's tooltip)
function getValidRegexp(aText, aTextbox) {
    if (!aText || (aText.length==0)) return;
    // wrap with /, this should also prevent from evaluation of any javascript
    if (aText[0]!='/') aText='/'+aText;
    try {
        var re = eval( aText );
        if (! re instanceof RegExp) {
            if (aTextbox) aTextbox.setAttribute("tooltiptext",getBundle().getString("xpather.wrnRegexpSyntax"));
            return;
        }
        if (aTextbox) aTextbox.setAttribute("tooltiptext", aTextbox.getAttribute("tooltiptext2"));
        return re;
    }
    catch(e) {
        if (aTextbox) aTextbox.setAttribute("tooltiptext",getBundle().getFormattedString("xpather.wrnRegexpSyntax2",[e]));
        return;
    }
}

//returns substitution or null
function getSubstition(){
    var s = document.getElementById('substText').value;
    return (!s || (s.length==0)) ? null : s;
}

//handles change of a regexp text (to perform syntax check)
function onRegexpTextChange(aTextbox){
    var text = aTextbox.value;
    if (getValidRegexp(text, aTextbox)) aTextbox.style.color = "black";
    else aTextbox.style.color = "red";
}

//polymorfic (xpather.xul)
function setSelectedNodeAsParent(){
    alert("todo");
}

//dispatches commands from the tree context menu
function onCmdContext(anEvent){
    var tree = document.getElementById('idExpTree');
    var cellXPath = treeView.fullXPaths[tree.currentIndex];
    var node = treeView.results[tree.currentIndex];
    switch(anEvent.target.id){
        case "ppCmdCopy":{
            getClipboard().copyString(cellXPath);
            break;
        }
        case "ppCmdBlink":{
            flashNode(node);
            break;
        }
        case "ppCmdXBox":{
            setXPathText(cellXPath);
            break;
        }
        case "ppCmdParentBox":{
            setParentNode(node, cellXPath);
            break;
        }
        case "ppCmdSelectInTree":{
            selectNodeInTree(node);
            break;
        }            
    }
}
