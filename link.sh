#!/bin/zsh

FIREFOX_PROFILE_DIR=${1:? Script takes single argument, path to firefox profile directory } 
UUID="{$(sed -n '/<Description about/,/version/{/em:id/{s_.*>{__;s_}.*__;p}}' < ./src/install.rdf)}"

#test -d $FIREFOX_PROFILE_DIR || echo "must provide profile path" > /dev/stderr 
#
if [ ! -d $FIREFOX_PROFILE_DIR ]
    then 
    echo "Specify the Firefox profile directory as the script argument" > /dev/stderr
    exit 1 
fi

if [ ! -f $1/prefs.js ]
 then
  echo "Specified Firefox directory does not contain prefs.js file" > /dev/stderr
  exit 1 
fi

EXTENSIONS_DIR=$FIREFOX_PROFILE_DIR/extensions

if [ ! -d $EXTENSIONS_DIR ]
 then
  echo "Specified Firefox directory does not contain extensions directory , creating" 
  echo "Creating $EXTENSIONS_DIR"
  mkdir -p  $EXTENSIONS_DIR
fi

CURRENT_DIR=$(pwd)
PROXY_FILE="$EXTENSIONS_DIR/$UUID"

cat <<END > $PROXY_FILE
$CURRENT_DIR/src
END

#636fd8b0-ce2b-4e00-b812-2afbe77ee899
